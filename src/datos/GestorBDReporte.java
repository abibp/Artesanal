package datos;

import negocio.entidades.ReporteDeVenta;

/**
 *
 * @author PIX
 */
public class GestorBDReporte extends GestorBD {

    public void agregarReporte(ReporteDeVenta nuevoReporte) {
    }

    public void eliminarReporte(int IDReporteAEliminar) {
    }

    public void editarInformacionReporte(int IDReporteAActualizar, ReporteDeVenta reporteActualizado) {
    }

    public ReporteDeVenta obtenerReporte(int IDReporteAObtener) {
        return null;
    }
}
